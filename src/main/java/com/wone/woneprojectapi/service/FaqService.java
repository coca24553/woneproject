package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Faq;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.enums.ResultCode;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.board.faq.*;
import com.wone.woneprojectapi.repository.FaqRepository;
import com.wone.woneprojectapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class FaqService {
    private final FaqRepository faqRepository;


    /**
     *
     * @param request 사용자가 문의사항 창에서 작성
     */
    // app 문의 사항 등록
    public void setFaq (Member member, FaqCreateRequest request){
        Faq addData = new Faq();
        addData.setMember(member);
        addData.setAskCreateDate(LocalDateTime.now());
        addData.setAskContent(request.getAskContent());
        addData.setPublicType(request.getPublicType());
        addData.setAskPassword(request.getAskPassword());

        faqRepository.save(addData);
    }

    // web 관리자 페이지 전체 조회
    public List<FaqAdminItem> getFaqsAdmin(){
        List<Faq> originList = faqRepository.findAll();

        List<FaqAdminItem> result = new LinkedList<>();

        for (Faq faq: originList){
            FaqAdminItem addItem = new FaqAdminItem();
            addItem.setId(faq.getId());
            addItem.setMemberName(faq.getMember().getUsername());
            addItem.setAskCreateDate(faq.getAskCreateDate().toLocalDate());
            addItem.setAskContent(faq.getAskContent());
            addItem.setPublicType(faq.getPublicType() ? "비공개":"공개");
            addItem.setComment(faq.getComment());

            result.add(addItem);
        }
        return result;
    }

    // 문의사항 app 전체 조회
    // 비공개 시 AskContent가 PublicType으로 변경 되게 수정이 필요
    public List<FaqAppItem> getFaqsApp(){
        List<Faq> originList = faqRepository.findAll();

        List<FaqAppItem> result = new LinkedList<>();

        for (Faq faq: originList){
            FaqAppItem addItem = new FaqAppItem();
            addItem.setId(faq.getId());
            addItem.setMemberName(faq.getMember().getUsername());
            addItem.setAskContent(faq.getAskContent());
            addItem.setPublicType(faq.getPublicType() ? "비공개":"공개");

            result.add(addItem);

        }
        return result;
    }

    // app에서 내 문의 글만 보기
    public List<FaqItem> getFaqUser(Member member){
        List<Faq> originList = faqRepository.findAllByMemberOrderByIdDesc(member);

        List<FaqItem> result = new LinkedList<>();

        for (Faq faq: originList){
            FaqItem addItem = new FaqItem();
            addItem.setId(faq.getId());
            addItem.setMemberName(faq.getMember().getUsername());
            addItem.setAskContent(faq.getAskContent());
            addItem.setPublicType(faq.getPublicType() ? "비공개":"공개");

            result.add(addItem);
        }
        return result;
    }


    // app, web 문의글 상세 보기[단수R]
    // oven에서는 상세보기 내역에서 글 작성이 완성된 화면이 따로 없으나 본인이 작성한 글을 확인하기 위해서 생성
    public FaqResponse getFaq(long id){
        Faq originData = faqRepository.findById(id).orElseThrow();

        FaqResponse response = new FaqResponse();
        response.setId(originData.getId());
        response.setMemberName(originData.getMember().getUsername());
        response.setAskCreateDate(originData.getAskCreateDate());
        response.setAskContent(originData.getAskContent());
        response.setPublicType(originData.getPublicType() ? "비공개":"공개");
        response.setComment(originData.getComment());
        response.setAskPassword(originData.getAskPassword());

        return response;
    }

    // 페이징
    public ListResult<Faq> getPaqPages(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum -1, 5);
        Page<Faq> faqs = faqRepository.findAll(pageRequest);

        ListResult<Faq> result = new ListResult<>();
        result.setMsg(ResultCode.SUCCESS.getMsg());
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setList(faqs.getContent());
        result.setTotalCount(faqs.getTotalElements());
        result.setTotalPage(faqs.getTotalPages());
        result.setCurrentPage(faqs.getPageable().getPageNumber() + 1);

        return result;
    }

    // web 관리자 페이지에서 문의 답변 수정
    public void putFaqAdmin(long id, FaqAdminChangeRequest request){
        Faq originData = faqRepository.findById(id).orElseThrow();

        originData.setComment(request.getComment());

        faqRepository.save(originData);
    }


    // app 사용자 문의사항 글 수정
    public void putFaqUser(long id, FaqUserChangeRequest request){
        Faq originData = faqRepository.findById(id).orElseThrow();

        originData.setAskCreateDate(LocalDateTime.now());
        originData.setAskContent(request.getAskContent());
        originData.setPublicType(request.getPublicType());
        originData.setAskPassword(request.getAskPassword());

        faqRepository.save(originData);
    }


    // app,web 문의 사항 삭제
    public void delFaq (long id){
        faqRepository.deleteById(id);
    }
}
