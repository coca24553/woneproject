package com.wone.woneprojectapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BoardType {
    NOTICE("공지사항"),
    GUIDE("이용방법");

    private final String boardType;
}
