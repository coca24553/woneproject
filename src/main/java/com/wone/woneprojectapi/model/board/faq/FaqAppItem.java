package com.wone.woneprojectapi.model.board.faq;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FaqAppItem {
    private Long id;
    private String memberName;
    private String askContent;
    private String publicType;
}
