package com.wone.woneprojectapi.model.board.notice;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardUseResponse {
    private Long id;
    private String boardType;
    private String boardTitle;
    private String boardContent;
}
