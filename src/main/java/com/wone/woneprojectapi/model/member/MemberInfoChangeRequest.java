package com.wone.woneprojectapi.model.member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MemberInfoChangeRequest {
    private String username;
    private LocalDate birthDate;
    private String password;
}
