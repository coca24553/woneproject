package com.wone.woneprojectapi.model.member;

import com.wone.woneprojectapi.enums.MemberStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class MemberOutChangeRequest {
    @Enumerated(value = EnumType.STRING)
    private MemberStatus memberStatus;

    private LocalDateTime outDate;
}
