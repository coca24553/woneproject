package com.wone.woneprojectapi.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class NearDayStatisticsResponse {
    private List<String> labels;
    private List<Long> datasets;
}
