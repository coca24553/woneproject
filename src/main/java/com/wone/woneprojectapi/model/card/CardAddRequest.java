package com.wone.woneprojectapi.model.card;

import com.wone.woneprojectapi.enums.CardGroup;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CardAddRequest {
    @Enumerated(value = EnumType.STRING)
    private CardGroup cardGroup;
    private String cardNumber;
    private LocalDate endDate;
    private Short cvc;
    private String etcMemo;
}
