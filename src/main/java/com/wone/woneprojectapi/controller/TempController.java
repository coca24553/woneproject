//package com.wone.woneprojectapi.controller;
//
//import com.wone.woneprojectapi.model.CommonResult;
//import com.wone.woneprojectapi.service.ResponseService;
//import com.wone.woneprojectapi.service.TempService;
//import lombok.RequiredArgsConstructor;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.IOException;
//
//@RestController
//@RequiredArgsConstructor
//@RequestMapping("/v1/temp")
//public class TempController {
//    private final TempService tempService;
//
//    @PostMapping("/file-upload")
//    public CommonResult setMemberListByFile(@RequestParam("csvFile")MultipartFile csvFile) throws IOException {
//        tempService.setMemberListByFile(csvFile);
//
//        return ResponseService.getSuccessResult();
//    }
//}
