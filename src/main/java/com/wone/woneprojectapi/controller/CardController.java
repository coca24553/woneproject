package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.card.*;
import com.wone.woneprojectapi.service.CardService;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/card")
public class CardController {
    private final CardService cardService;
    private final MemberService memberService;

    // 카드 등록
    @PostMapping("/new/memberId/{memberId}")
    @Operation(summary = "카드 등록")
    public CommonResult setCard(@PathVariable long memberId, @RequestBody CardAddRequest request){
        Member member = memberService.getData(memberId);
        cardService.setCard(member, request);

        return ResponseService.getSuccessResult();
    }

    // 카드 전체 조회
    @GetMapping("/detail/{id}")
    @Operation(summary = "카드 전체 조회")
    public SingleResult<CardResponse> getCard(@PathVariable long id){
        return ResponseService.getSingleResult(cardService.getCard(id));
    }

    // 카드 멤버별 조회
    @GetMapping("/all/member-id/{memberId}")
    @Operation(summary = "카드 멤버별 조회")
    public ListResult<CardItem> getCardMember(@PathVariable long memberId){
        Member member = memberService.getData(memberId);
        return ResponseService.getListResult(cardService.getCardMembers(member));
    }

//    // 사용자 카드 등록 갯수 체크(1장 이상 등록 하지 못하게 체크)
//    @GetMapping("/check")
//    public CardDupCheckResponse getCardCheck(@RequestParam(name = "memberId") Member member){
//        return cardService.getCardCheck(member);
//    }

    // 카드 정보 수정1 (카드번호, cvc, 유효기간)
    @PutMapping("/info/change/{id}")
    @Operation(summary = "카드 정보 수정1 (카드번호, cvc, 유효기간)")
    public CommonResult putCardChange(@PathVariable long id, @RequestBody CardChangeRequest request){
        cardService.putCardChange(id, request);

        return ResponseService.getSuccessResult();
    }

    // 카드 정보 수정2 (카드사)
    @PutMapping("/info/group-change/{id}")
    @Operation(summary = "카드 정보 수정2 (카드사)")
    public CommonResult putCardGroupChange(@PathVariable long id, @RequestBody CardGroupChangeRequest request){
        cardService.putCardGroupChange(id, request);

        return ResponseService.getSuccessResult();
    }

    // 카드 삭제
    @DeleteMapping("/delete/{id}")
    @Operation(summary = "카드 삭제")
    public CommonResult delCard(@PathVariable long id){
        cardService.delCard(id);

        return ResponseService.getSuccessResult();
    }


}
