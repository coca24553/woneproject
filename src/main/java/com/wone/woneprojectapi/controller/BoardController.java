package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.board.notice.*;
import com.wone.woneprojectapi.service.BoardService;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;
    private final MemberService memberService;

    @PostMapping("/new/member-id/{memberId}")
    @Operation(summary = "공지사항, 이용방법 게시판 등록")
    public CommonResult setBoard (@PathVariable long memberId, @RequestBody BoardCreateRequest request){
        Member member = memberService.getData(memberId);
        boardService.setBoard(member, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/notice/all")
    @Operation(summary = "공지사항만 조회")
    public ListResult<BoardNoticeItem> getNotices (){
        return ResponseService.getListResult(boardService.getNotices());
    }

    @GetMapping("/use/all")
    @Operation(summary = "게시판 이용방법만 모두 조회")
    public ListResult<BoardUseItem> getUseBoards (){
        return ResponseService.getListResult(boardService.getUseBoards());
    }

    @GetMapping("/detail/notice/{id}")
    @Operation(summary = "게시판(공지사항) 내용 상세보기")
    public SingleResult<BoardNoticeResponse> getNoticeBoard(@PathVariable long id){
        return ResponseService.getSingleResult(boardService.getNoticeBoard(id));
    }

    @GetMapping("/detail/use/{id}")
    @Operation(summary = "이용방법 상세보기 (등록날짜 제거 ver)")
    public SingleResult<BoardUseResponse> getUesBoard(@PathVariable long id){
        return ResponseService.getSingleResult(boardService.getUseBoard(id));
    }

    @GetMapping("/all/notice/{pageNum}")
    @Operation(summary = "공지사항 페이징")
    public ListResult<BoardNoticeItem> getNoticePages(@PathVariable int pageNum) {
        return boardService.getNoticePages(pageNum);
    }

    @GetMapping("/all/use-board/{pageNum}")
    @Operation(summary = "이용방법 페이징")
    public ListResult<BoardUseItem> getUseBoards(@PathVariable int pageNum) {
        return boardService.getUseBoardPages(pageNum);
    }

    @PutMapping("/change-content/{id}")
    @Operation(summary = "게시판 내용 수정하기")
    public CommonResult putBoard(@PathVariable long id, @RequestBody BoardContentChangeRequest request){
        boardService.putBoard(id, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "게시글 삭제하기")
    public CommonResult delBoard(@PathVariable long id){
        boardService.delBoard(id);

        return ResponseService.getSuccessResult();
    }

}
