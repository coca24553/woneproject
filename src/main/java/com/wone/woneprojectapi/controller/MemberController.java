package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.NearDayStatisticsResponse;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.member.*;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    // csv
    @PostMapping("/file-upload")
    @Operation(summary = "csv 업로드")
    public CommonResult setMemberListByFile(@RequestParam("csvFile")MultipartFile csvFile) throws IOException {
        memberService.setMemberListByFile(csvFile);

        return ResponseService.getSuccessResult();
    }

    // 사용자 회원가입
    @PostMapping("/join")
    @Operation(summary = "사용자 회원가입")
    public CommonResult setMemberJoin(@Valid @RequestBody MemberJoinRequest request) throws Exception {
        memberService.setMemberJoin(request);
        return ResponseService.getSuccessResult();
    }

    // 회원가입 id 중복 체크
    @GetMapping("/check/id")
    @Operation(summary = "회원가입 id 중복 체크")
    public MemberDupCheckResponse getMemberDupCheck(
            @RequestParam(name = "userId") String username){
        return memberService.getMemberIdDupCheck(username);
    }

    // WEB 사용자 전체 조회
    @GetMapping("/all")
    @Operation(summary = "WEB 사용자 전체 조회")
    public ListResult<MemberItem> getMembers(){
        return ResponseService.getListResult(memberService.getMembers());
    }

    // WEB 관리자 페이지 사용자 상세 조회
    @GetMapping("/detail/admin/{id}")
    @Operation(summary = "WEB 관리자 페이지 사용자 상세 조회")
    public SingleResult<MemberResponse> getMemberAdmin(@PathVariable long id){
        return ResponseService.getSingleResult(memberService.getMemberAdmin(id));
    }

    // APP 사용자 상세 조회
    @GetMapping("/detail/{id}")
    @Operation(summary = "APP 사용자 상세 조회")
    public SingleResult<MemberResponse> getMember(@PathVariable long id){
        return ResponseService.getSingleResult(memberService.getMember(id));
    }

    // 가입자 수 조회
    @GetMapping("/total/count")
    @Operation(summary = "가입자 수 조회")
    public SingleResult<NearDayStatisticsResponse> getNearDayStatistics() {
        return ResponseService.getSingleResult(memberService.getNearDayStatistics());
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "member 페이징")
    public ListResult<Member> getMemberPages(@PathVariable int pageNum) {
        return memberService.getMemberPages(pageNum);
    }

    // APP 사용자 정보 수정
    @PutMapping("/info/change/{id}")
    @Operation(summary = "APP 사용자 정보 수정")
    public CommonResult putMemberInfo(@PathVariable long id, @RequestBody MemberInfoChangeRequest request){
        memberService.putMemberInfo(id, request);
        return ResponseService.getSuccessResult();
    }

    // APP 사용자 탈퇴
    @PutMapping("/out/{id}")
    @Operation(summary = "APP 사용자 탈퇴")
    public CommonResult putMemberOut(@PathVariable long id, @RequestBody MemberOutChangeRequest request){
        memberService.putMemberOut(id, request);
        return ResponseService.getSuccessResult();
    }
}
