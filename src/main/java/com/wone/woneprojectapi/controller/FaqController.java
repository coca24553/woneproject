package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Faq;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.board.faq.*;
import com.wone.woneprojectapi.service.FaqService;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/faq")
public class FaqController {
    private final MemberService memberService;
    private final FaqService faqService;

    @PostMapping("/new/member-id/{memberId}")
    @Operation(summary = "app 문의 사항 등록")
    public CommonResult setFaq (@PathVariable long memberId, @RequestBody FaqCreateRequest request){
        Member member = memberService.getData(memberId);
        faqService.setFaq(member, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/list/admin")
    @Operation(summary = "web 관리자 페이지 전체 조회")
    public ListResult<FaqAdminItem> getFaqsAdmin (){
        return ResponseService.getListResult(faqService.getFaqsAdmin());
    }

    @GetMapping("/list/app")
    @Operation(summary = "문의사항 app 전체 조회")
    public ListResult<FaqAppItem> getFaqsApp (){
        return ResponseService.getListResult(faqService.getFaqsApp());
    }

    @GetMapping("/detail/{id}")
    @Operation(summary = "사용자 문의 상세보기")
    public SingleResult<FaqResponse> getFaq(@PathVariable long id){
        return ResponseService.getSingleResult(faqService.getFaq(id));
    }

    @GetMapping("/contents/member-id/{memberId}")
    @Operation(summary = "app에서 내 문의 글만 보기")
    public ListResult<FaqItem> getFaqUser (@PathVariable long memberId) {
        Member member = memberService.getData(memberId);

        return ResponseService.getListResult(faqService.getFaqUser(member));
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "문의사항 페이징")
    public ListResult<Faq> getPaqPages(@PathVariable int pageNum) {
        return faqService.getPaqPages(pageNum);
    }

    @PutMapping("/comment/{id}")
    @Operation(summary = "web 관리자 페이지에서 문의 답변 수정")
    public CommonResult putFaqAdmin(@PathVariable long id, @RequestBody FaqAdminChangeRequest request){
        faqService.putFaqAdmin(id, request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change-faq/{id}")
    @Operation(summary = "app 사용자 문의사항 글 수정")
    public CommonResult putFaqUser(@PathVariable long id, @RequestBody FaqUserChangeRequest request ){
        faqService.putFaqUser(id, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "app,web 문의 사항 삭제")
    public CommonResult delFaq(@PathVariable long id){
        faqService.delFaq(id);

        return ResponseService.getSuccessResult();
    }
}
