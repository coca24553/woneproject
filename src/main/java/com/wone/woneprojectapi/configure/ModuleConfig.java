package com.wone.woneprojectapi.configure;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.format.DateTimeFormatter;

@Configuration
public class ModuleConfig {
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {

        return builder -> {
            // LocalDate 타입 직렬화, 역직렬화 패턴
            DateTimeFormatter localTimeSerializeFormatter = DateTimeFormatter.ofPattern("HH:mm");
            DateTimeFormatter localTimeDeserializeFormatter = DateTimeFormatter.ofPattern("HH:mm");

            // 등록
            builder.serializers(new LocalDateTimeSerializer(localTimeSerializeFormatter));
            builder.deserializers(new LocalDateTimeDeserializer(localTimeDeserializeFormatter));

        };
    }
}
