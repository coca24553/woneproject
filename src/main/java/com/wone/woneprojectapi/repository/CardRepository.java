package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Card;
import com.wone.woneprojectapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CardRepository extends JpaRepository<Card, Long> {

    List<Card> findAllByMember (Member member) ;

    long countByMember(Member member);
}
