package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface MemberRepository extends JpaRepository <Member, Long> {
    // id 중복확인
    // userId = unique
    // userid와 일치하는 갯수를 알고 싶을 때
    long countByUserId(String userId);

    // 가입자 수 구하기 (일주일 기간 한정)
    long countByJoinDateBetween(LocalDateTime startTime, LocalDateTime endTime);
}